# Manipulations proposées pour cette demi-journée de manipulations

## Manipulations de base 

- Accéder au projet Bac-A-Sable
- Pour les commits, renseignez quelques informations pour que l'on puisse identifier la personne qui les a réalisés
- Créer un répertoire, via l'interface graphique, avec un nom que vous choisirez
- Créer un fichier dans ce répertoire
- Retrouvez ensuite les commits dans l'interface de gitlab


## Manipulations pour l'utilisation d'un dépot git local

L'installation de git en local permet d'avoir une copie en local. 

On va devoir installer les logiciels git et vscodium pour l'edition en markdown

Pour cela, plutôt utiliser wapt à l'adresse http://wapt-server.mmsh.fr/ pour récupérer les logiciels git et vscodium

## Manipulations avancées autour de gitlab 

Issues, Branches, Milestones, etc.  

## Manipulations pour l'intégration continue 

Se connecter sur le projet "Projet Decouverte Gitlab 12 Decembre". 

- Repérer les étapes CI/CD et les pipelines. 
- Retrouver dans le fichier mkdocs.yml les éléments du menu
- Créer une page de garde qui s'insérera en premier dans les pages du projet
- Créer une issue suppression de la page index.md du projet 
- ou une autre issue suppression de la page about.md 
- ou une troisième pour l'insertion de la page garde.md
- s'affecter l'issue et la réaliser
